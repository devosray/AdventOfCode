#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

class Board
{
	struct pos {
		int x, y;
	};

	char grid[5][5] =
	{
		{ ' ', ' ', '5', ' ', ' ' } ,
		{ ' ', '2', '6', 'A', ' ' } ,
		{ '1', '3', '7', 'B', 'D' } ,
		{ ' ', '4', '8', 'C', ' ' } ,
		{ ' ', ' ', '9', ' ', ' ' } ,
	};
	
	pos currentPos;
			
	public:	
		Board() 
		{
			currentPos.x = 0;
			currentPos.y = 2;
		}

		bool isValid()
		{
			if (currentPos.x < 5 && currentPos.x >= 0 && currentPos.y >= 0 && currentPos.y < 5)
				return getCurrent() != ' ';
			else
				return false;
		}

		void up()
		{
			currentPos.y--;
			if (!isValid())
				currentPos.y++;
		}

		void down()
		{
			currentPos.y++;
			if (!isValid())
				currentPos.y--;
		}

		void left()
		{
			currentPos.x--;
			if (!isValid())
				currentPos.x++;
		}

		void right()
		{
			currentPos.x++;
			if (!isValid())
				currentPos.x--;
		}

		char getCurrent()
		{
			return grid[currentPos.x][currentPos.y];
		}

		friend std::ostream& operator<<(std::ostream& stream, const Board& board)
		{
			for (int i = 0; i < (3 * 2 + 1) * 5; i++)
				stream << "-";
			stream << std::endl;

			for (int y = 0; y < 5; y++)
			{
				for (int x = 0; x < 5; x++)
				{
					bool cur = (board.currentPos.x == x && board.currentPos.y == y);
					if (cur)
						stream << " [";
					else
						stream << "  ";

					stream << board.grid[x][y];
					
					if (cur)
						stream << "] ";
					else
						stream << "  ";
				}
				stream << std::endl;
			}
			return stream;
		}
};

using namespace std;

int main() 
{
	Board board;
	cout << board << endl;

	ifstream inFile("data/input.txt");
	string line;
	stringstream code("");
	
	while (getline(inFile, line))
	{
		for (auto it = line.begin(); it != line.end(); ++it)
		{
			char c = *it;
			switch (c)
			{
				case 'L':
					board.left();
					break;
				case 'R':
					board.right();
					break;
				case 'U':
					board.up();
					break;
				case 'D':
					board.down();
			}			
		}

		cout << board << endl;
		code << board.getCurrent();
	}

	cout << "Final Code: " << code.str() << endl;
	cout << "TEST" << endl;
	return 0;
}